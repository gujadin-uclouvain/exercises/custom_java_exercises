package basic_structures;

import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.NoSuchElementException;

public class ArrayQueue<E> implements Iterable<E> {

    private E[] array = (E[]) new Object[10];
    private int size = 0;       // Size of the queue
    private int nOp = 0;        // Number of operation

    private int start = 0;      // Index of the first element in the queue
    private int end = 0;        // Index of the last element in the queue


    public int size() {
        return size;
    }

    public boolean isEmpty() {
        return size() == 0;
    }

    /**
     * Inserts the specified element into this queue, returning
     * {@code true} upon success.
     *
     * @param e the element to add
     * @return {@code true}
     */
    public boolean add(E e) {
        return false;
    }

    /**
     * Retrieves and removes the head of this queue.
     *
     * @return the head of this queue
     * @throws NoSuchElementException if this queue is empty
     */
    public E remove() {
        return null;
    }

    /**
     * Retrieves, but does not remove, the head of this queue,
     * or returns {@code null} if this queue is empty.
     *
     * @return the head of this queue, or {@code null} if this queue is empty
     */
    public E peek() {
        return null;
    }

    /**
     * Returns an iterator over the ordered elements in this collection.
     *
     * @return an {@code Iterator} over the ordered elements in this collection
     */
    public Iterator<E> iterator() {
        return new Iterator<E>() {

            @Override
            public boolean hasNext() {
                return false;
            }

            /**
             * Returns the next element in the iterator.
             *
             * @return an {@code Iterator} over the ordered elements in this collection
             * @throws NoSuchElementException if the iteration has no more elements
             * @throws ConcurrentModificationException if an element has been added or removed from the queue
             */
            @Override
            public E next() throws NoSuchElementException, ConcurrentModificationException {
                return null;
            }
        };
    }
}
