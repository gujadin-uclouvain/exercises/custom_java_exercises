package tools;

public class PairTest<U, T> {
    private final U user;
    private final T test;

    public PairTest(U user, T test) {
        this.user = user;
        this.test = test;
    }

    public T getTest() { return test; }
    public U getUser() { return user; }
}
