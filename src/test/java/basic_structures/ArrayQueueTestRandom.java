package basic_structures;

import org.junit.Test;
import tools.PairTest;

import java.util.*;

import static org.junit.Assert.*;

public class ArrayQueueTestRandom {

    @Test
    public void runAsExpected() {
        PairTest<ArrayQueue<Integer>, Queue<Integer>> data = populateRndIntData();
        Iterator<Integer> aIter = data.getUser().iterator();
        Iterator<Integer> bIter = data.getTest().iterator();
        assertEquals("Structures should have the same size", data.getTest().size(),data.getUser().size());
        while (bIter.hasNext()) {
            assertTrue("Created structure should have an element", aIter.hasNext());
            assertEquals("Structures should have the same element", bIter.next(),aIter.next());
        }
        assertFalse(bIter.hasNext());
        assertFalse("Created structure should have no more element left", aIter.hasNext());
    }

    // Data Generation
    public PairTest<ArrayQueue<Integer>, Queue<Integer>> populateRndIntData() {
        Random r = new Random();
        ArrayQueue<Integer> a = new ArrayQueue<>();
        Queue<Integer> b = new LinkedList<>();
        int n = r.nextInt(20, 100);
        for (int i = 0; i < n; i++) {
            int v = r.nextInt();
            a.add(v);
            b.add(v);
        }
        return new PairTest<>(a, b);
    }
}
